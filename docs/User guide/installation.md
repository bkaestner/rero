# Installation

Rero requires Python 3.5 or newer to run.

## Stable version
To install Rero, run this command in your terminal:

``` bash
$ pip install rero
```

This is the preferred method to install Rero, as it will always install the most recent stable release.

If you don't have [pip](https://pip.pypa.io) installed, this [Python installation guide](http://docs.python-guide.org/en/latest/starting/installation/) can guide
you through the process.

## From sources

The sources for Rero can be downloaded from the [Gitlab repository](https://gitlab.com/PaniR/rero).

You can clone the public repository:

``` bash
$ git clone https://gitlab.com/PaniR/rero.git
```

Once you have a copy of the source, you can install it with:
```bash
$ python setup.py install
```
or
```bash
$ python poetry install
```
For the latter option have a look on [Poetry](https://poetry.eustace.io/docs/cli/#install).
