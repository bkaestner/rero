# Configuration

In order to be able to configure Rero the way you need it, you have to understand some of the mechanics of how it works. This configuration page is therefore a mixture of explanations and configuration options.

## How it works

A Rero frame is packed the following way:

| Shared code/length byte | additional length bytes | length field checksum  | address bytes | payload bytes | CRC bytes | EOF delimiter (0x00) |
|-------------------------|---------------------------|-----------------|---------|-----|----------------------|----------|

The payload is encoded using COBSR, whereas the header + CRC field is independently encoded using COBS.
The purpose of the individual fields and their limits are listed in the following table:

| Field                   | Purpose                                                   | Symbol | Limit     |
|-------------------------|-----------------------------------------------------------|:------:|:---------:|
| Shared code/length byte | Holds first COBS code and part of frame length field      |    -   |     -     |
| Additional length bytes | Optional expansion of length field                        |   $k$  |   $0-3$   |
| Length field checksum   | Optional checksum of length field for enhanced robustness |   $c$  |   $0-1$   |
| Address bytes           | Optional address field                                    |   $l$  |   $0-4$   |
| Payload bytes           | COBSR encoded payload                                     |   $p$  | \(p_{\mathrm{max}}\)  |
| CRC bytes               | Optional frame check sequence                             |   $m$  |   $0-8$   |
| EOF delimiter           | End of frame (EOF) delimiter (0x00) marks end of frame    |    -   |     -     |

Rero makes use of every single bit available. As such, it determines how many bits the first COBS code needs and uses the
rest of the first byte to extend the length field. The maximum length of the first COBS code is restricted to 4 bit. This
results in a small header overhead with reasonable big header + CRC fields.
The length field is required to allow for a reduction of processing overhead. The optional 
length field checksum (8-bit [ones complement checksum](https://en.wikibooks.org/wiki/Communication_Networks/TCP_and_UDP_Protocols/UDP#Checksum_Calculation)) enhances robustness against a corrupted length field.
All optional fields are disabled by setting their corresponding lengths to zero.

There is a certain **limit** concerning the total number of header + CRC bytes:

<a name="pmax"></a>
$$
k+c+l+m \le 13.
$$

Due to the limited number of header bytes, there is an upper limit for the maximum payload length $p_{\mathrm{max}}$.
It is determined as

$$
p_{\mathrm{max}}=2^{8(k+1)-n}-1
$$

$$
n=\mathrm{ceil}\left(\mathrm{log}_{2}\left(k+c+l+m+2\right)\right),
$$
where $n$ is the number of bits needed to represent the first COBS code.
A complete list of the possible configurations of Rero can be found in the [valid settings](#valid-settings) section.

The sequence of packing is as follows:

1. The payload is encoded using COBSR
2. The CRC checksum is computed from the address + encoded payload
3. The rest of the header is determined and header + CRC are encoded using COBS

This approach allows for the following efficiency advantages:

* Encoded payload can be CRC checked without decoding it, hence in case of a failed CRC check the decoding step can be omitted
* Through COBS and COBSR, the end of frame (EOF) delimiter (0x00) can be found without checking every byte of the frame individually
* Encoded payload can be decoded piecewise and as such the payload bytes need to be read only once when needed

In summary this approach is perfectly suited for MCUs with DMA support, since for the decoding of the frame not every single
byte in the frame need to be checked and the purpose of the DMA (not to read individual bytes twice) 
is not foiled. If you need more information on how it works, have a look into the [further details for the interested reader](#further-details-for-the-interested-reader)
section.

Currently Rero is available as a Python module and a C implementation for an STM32 MCU.

## Valid settings

The following table lists the maximum possible payload length $p_{\mathrm{max}}$ in bytes for all combinations of
additional length field bytes $k$ and the total length of the header + CRC checksum $k+c+l+m$.

|$k$|$k+c+l+m$|$p_{\mathrm{max}}$|
|:-:|:--:|:-------------------:|
| 0 |  0 |          63         |
| 0 |  1 |          63         |
| 1 |  1 |        16383        |
| 0 |  2 |          31         |
| 1 |  2 |         8191        |
| 2 |  2 |       2097151       |
| 0 |  3 |          31         |
| 1 |  3 |         8191        |
| 2 |  3 |       2097151       |
| 3 |  3 |      536870911      |
| 0 |  4 |          31         |
| 1 |  4 |         8191        |
| 2 |  4 |       2097151       |
| 3 |  4 |      536870911      |
| 0 |  5 |          31         |
| 1 |  5 |         8191        |
| 2 |  5 |       2097151       |
| 3 |  5 |      536870911      |
| 0 |  6 |          15         |
| 1 |  6 |         4095        |
| 2 |  6 |       1048575       |
| 3 |  6 |      268435455      |
| 0 |  7 |          15         |
| 1 |  7 |         4095        |
| 2 |  7 |       1048575       |
| 3 |  7 |      268435455      |
| 0 |  8 |          15         |
| 1 |  8 |         4095        |
| 2 |  8 |       1048575       |
| 3 |  8 |      268435455      |
| 0 |  9 |          15         |
| 1 |  9 |         4095        |
| 2 |  9 |       1048575       |
| 3 |  9 |      268435455      |
| 0 | 10 |          15         |
| 1 | 10 |         4095        |
| 2 | 10 |       1048575       |
| 3 | 10 |      268435455      |
| 0 | 11 |          15         |
| 1 | 11 |         4095        |
| 2 | 11 |       1048575       |
| 3 | 11 |      268435455      |
| 0 | 12 |          15         |
| 1 | 12 |         4095        |
| 2 | 12 |       1048575       |
| 3 | 12 |      268435455      |
| 0 | 13 |          15         |
| 1 | 13 |         4095        |
| 2 | 13 |       1048575       |
| 3 | 13 |      268435455      |

## Further details for the interested reader

This section holds more details about the individual fields and their purpose for the interested reader.

### Shared code length byte:
As mentioned above the shared code/length byte, the additional frame length field, the address field and the CRC
field are together encoded using plain COBS. Plain COBS because we know exactly how long the encoded result is.
In this case we always get one additional overhead byte holding the COBS code. The value of this code tells us
where the next 0x00 byte is located (have a look on how [COBS](https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing) works). The maximum value of the COBS code is
determined by the number of header + CRC bytes we use. We limit this maximum number to 13, which guarantees us that
we do not need more than 4 bits for the COBS code. We use the remaining 4 bits as part of the length field. Using
this approach we get reasonable maximum payload lengths and header + CRC lengths while using every bit available.

### Frame length field:
A traditional approach when using byte stuffed communication is to determine starts and stops of frames by
checking every individual byte for and EOF delimiter. However, in case you use a DMA, all your bytes are
transferred to your buffer without any necessary intervention. Checking every individual byte afterwards in your
buffer for an EOF delimiter is a nasty processing step. In order to avoid that, we introduce a length field,
telling us where we have to look for the EOF delimiter. In case the EOF delimiter is at the given index,
synchronization is given. In case not, we lost synchronization and need to resynchronize, which is easy (well this
time we really need to check every individual byte but well at least we can recover).

### Optional frame length field checksum:
The frame length field is the only important field which is not covered by  a checksum by default. In case of an
error manipulating the length field two things may happen. In case the manipulated length is shorter than the
true one, the error will be detected very soon since the EOF delimiter will not be at the place given. In this
case we will be synchronized again soon. The second case holds the worst case, if the length field is manipulated
to the maximum frame length, we may have to wait very long until this number of bytes is received. In this case a
lot of bytes may be lost. One thing which may help you out of that is a timeout. In case no more bytes are
received this will notify you that something went wrong. In case of transmit/reply communication this would work.
However, in case of a byte stream the timeout will not be triggered. For this case an additional checksum byte
alone for the length field can be enabled. The one's complement checksum is used since a CRC may be too
computationally complex for this task. By use of this checksum an error will be detected sooner and thus less
frames lost.

### Address field:
In case of multi node applications (like buses) address bytes give you the possibility to address your destination.

### Payload:
What you want transmitted. Rero supports also empty frames, where no payload is transmitted. This may be used for
signaling purposes.

### CRC Field:
The CRC checksum bytes of the payload are traditionally located at the end of the frame. This is useful if
the CRC code used can be appended to the payload making the CRC checksum calculated from the combination results
to zero. This does not work for every CRC calculation procedure, however, very often such a CRC is implemented in
MCU units. This facilitates the verification step (especially if a CRC hardware module is present in your MCU).
Since we use a length field telling us the length of the encoded payload we know where the CRC field begins. As
such we can find the CRC field, decode it and verify the still encoded payload. Traditionally, the CRC code is
appended to the payload and encoded together with it. As such it is mandatory to decode the whole frame before being
able to verify it. In case the CRC check fails, we can omit the decoding step at all.

### In summary:
Rero comes in handy in case the frame is processed using a micro controller, especially if a DMA is used. The
main goal of Rero is to minimize the processing overhead of the frame handling. As such, the frame can be CRC
checked without decoding it. This saves processing overhead in case CRC check fails. Furthermore,
the CRC calculation can be started before the frame was received completely (which, however, is not done in Python or the C implementation). The
shared length field enables the detection of the EOF delimiter without checking every byte in the frame
individually. In case the EOF delimiter is not found at the location given by the length field, the communication
is assumed to be de-synchronized. Synchronization can be re-established by scanning for an EOF delimiter in
subsequent received frames. This works since frames packed by Rero do not contain any EOF delimiter except at the
end of a packed frame. If a received frame was successfully CRC checked, the still encoded payload can be decoded
piecewise and as such the payload is read only once when it is needed and not twice as would be required for a
preceding decoding (which is not done in Python but available in the C implementation).
