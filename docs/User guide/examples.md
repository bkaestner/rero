# Examples

## Synchronous implementation

```Python
import serial 
import rero
import logging
import crcmod

# Setup logging
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# Setup rero
reroPacker = rero.Packer(addrSrc=99, addrDstDefault=1, crc=crcmod.predefined.Crc('crc-32-mpeg'), addrFieldLen=2,
						 addFrameFieldLen=1, enableFrameLenCheck=True, yieldCRCError=True,
						 yieldOtherRecipientNotification=True, logHandler=ch)

if __name__ == "__main__":

    ser = serial.Serial()
    ser.baudrate = 38400
    ser.port = 'COM1'
    ser.timeout = 1
    ser.open()

    # Dummy message
    payload = bytes([1, 2, 3, 0, 4, 5, 6, 0, 7, 8, 1])
    msg = reroPacker.pack(payload, 1)    # We skip the packing error checking for now

    # Send
    ser.write(msg)
    print('Sent message...')
    print(payload)

    # Receive answer from MCU
    msgRcd = ser.read_until(b'0')

    if msgRcd != b'':

        print('Answer received...')
        print(msgRcd)

        # Unpack received message - it is assumed a complete frame is received including EOF delimiter (0x00)
        try:
            for payloadRcd in reroPacker.unpack(msgRcd):

                # Since we enabled yielding of errors we need to check if stuff returned is of an error type
                if isinstance(payloadRcd, rero.ReroError):
                    raise payloadRcd

                print(payloadRcd)

                # Check if received frame is the same
                if payload == payloadRcd:
                    print('OK')
                else:
                    print('False')
        
        # In case we would like to continue receiving we would need to restart msgRcd = ser.read_until()...
        except rero.OtherRecipientReroNotification as e:
            print(e)
        except rero.CRCReroError as e:
            print(e)
        except rero.OutOfSyncReroError as e:
            # Resynchronize by scanning for EOF delimiter in subsequent received frames
            print(e)
    else:
        print('Nothing received!')

    ser.close()
```

##	 Asynchronous implementation
```Python
import asyncio
import aioserial
import rero
import logging
import crcmod


async def aio_recv_and_unpack(ser: aioserial.AioSerial, reroPacker: rero.Packer) -> bytes:
    """Async. read of all received bytes and unpack."""

    payload = b''

    while not payload:

        # Await the first byte
        msg = await ser.read_async()

        # Check if there is more to read
        msg = msg + ser.read(ser.in_waiting)

        # Unpack and return
        for payload in reroPacker.unpack(msg):

            # Since we enabled yielding of errors we need to check if stuff returned is of an error type
            if isinstance(payload, rero.ReroError):
                raise payload

    return payload


async def main(ser: aioserial.AioSerial, reroPacker: rero.Packer) -> None:
    # Communication testing messages

    # Normal message where COBS applies
    payload = bytes([1, 2, 3, 0, 4, 5, 6, 0, 7, 8, 1])
    msgCOBS = reroPacker.pack(payload, 1)    # We skip the packing error checking for now

    # Send
    ser.write(msgCOBS)
    print('Sent normal message COBS...')

    # Receive answer from MCU
    try:
        payloadRcd = await asyncio.wait_for(aio_recv_and_unpack(ser, reroPacker), 1)     # timeout 1 sec
        print('Answer received...')
        # Check if received frame is the same
        if payload == payloadRcd:
            print('Success!')
        else:
            print('Test failed!')
    except asyncio.TimeoutError:
        print('No answer received!')

    except rero.ReroError as e:
        print(e)    # in case we would like to continue receiving we would need to restart await asyncio.wait_for()... 


if __name__ == "__main__":

    import sys

    assert sys.version_info >= (3, 7), "Script requires Python 3.7+."

    # Setup logging
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # Setup rero
    reroPacker = rero.Packer(addrSrc=99, addrDstDefault=1, crc=crcmod.predefined.Crc('crc-32-mpeg'), addrFieldLen=2,
							addFrameFieldLen=1, enableFrameLenCheck=True, yieldCRCError=True,
							yieldOtherRecipientNotification=True, logHandler=ch)

    ser = aioserial.AioSerial()
    ser.baudrate = 38400
    ser.port = 'COM1'
	
    with ser as ser:
        asyncio.run(main(ser, reroPacker))
```


