# MIT License

# Copyright (c) 2019 Reinhard Panhuber

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import serial
import logging
import crcmod

try:
    import rero
except ImportError:
    from src.rero import packer as rero     # to work in PyCharm locally

# Setup logging
# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# Setup rero
reroPacker = rero.Packer(addrSrc=99, addrDstDefault=1, crc=crcmod.predefined.Crc('crc-32-mpeg'), addrFieldLen=2,
                         addFrameFieldLen=1, enableFrameLenCheck=True, yieldCRCError=True,
                         yieldOtherRecipientNotification=True, logHandler=ch)

if __name__ == "__main__":

    ser = serial.Serial()
    ser.baudrate = 38400
    ser.port = 'COM6'       # PC
    # ser.port = 'COM3'  # Laptop
    ser.timeout = 1
    ser.xonxoff = False
    ser.open()

    # Communication testing messages

    # Normal message where COBS applies
    payload = bytes([1, 2, 3, 0, 4, 5, 6, 0, 7, 8, 1])
    msgCOBS = reroPacker.pack(payload, 1)

    # Send
    # with ser as ser:
    ser.write(msgCOBS)
    print('Sent normal message COBS...')
    print(payload)

    # Receive answer from MCU
    # with ser as ser:
    msgRcd = ser.read_until(b'0')

    if msgRcd != b'':

        print('Answer received...')
        print(msgRcd)

        # Unpack received message - it is assumed a complete frame is received including EOF delimiter (0x00)
        try:
            for payloadRcd in reroPacker.unpack(msgRcd):

                # Since we enabled yielding of errors we need to check if stuff returned is of an error type
                if isinstance(payloadRcd, rero.ReroError):
                    raise payloadRcd

                print(payloadRcd)

                # Check if received frame is the same
                if payload == payloadRcd:
                    print('OK')
                else:
                    print('False')

        except rero.OtherRecipientReroNotification as e:
            print(e)
        except rero.CRCReroError as e:
            print(e)
        except rero.OutOfSyncReroError as e:
            # Resynchronize by scanning for EOF delimiter in subsequent received frames
            print(e)
    else:
        print('Nothing received!')

    ser.close()
