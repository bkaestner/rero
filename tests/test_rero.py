# MIT License

# Copyright (c) 2019 Reinhard Panhuber

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""This is a unit test for rero
To make this work in PyCharm, you have to INSTALL the package (at least once) by

`poetry build`

followed by

`poetry install`

Only then it will work with this special folder structure! The Python interpreter
needs the module installed, otherwise it is not able to find it! PyCharm finds the
module since the path is given relative to the project root.

HINT:
For poetry to not install a seperate environment (which is not good in case of
anaconda) do

`poetry config settings.virtualenvs.create false`

before any other poetry command!

"""

from unittest import TestCase
import logging

try:
    from src.rero import packer as rero
except ImportError:
    import rero

import crcmod
from time import sleep


class TestRero(TestCase):
    def test_pack(self):

        # Normal usage
        reRoCoder = rero.Packer(1, 10)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        self.assertEqual(msgToSend, b'\x17\x05\x01\x05\x01\x02\x03\x04\xf6\x98\xbf\xb2\x00')

        # Overlength exception
        payload = bytes(4079)
        with self.assertRaises(rero.PayloadOverlengthReroError):
            reRoCoder.pack(payload)

        # No address exception
        reRoCoder = rero.Packer(1)
        payload = bytes([1, 2, 3, 4])
        with self.assertRaises(rero.NoDestinationAddressReroError):
            reRoCoder.pack(payload)

        # Provoke address field over length
        with self.assertRaises(ValueError):
            rero.Packer(1, addrFieldLen=5)

        # Provoke addFrameFieldLen over length
        with self.assertRaises(ValueError):
            rero.Packer(1, addFrameFieldLen=4)

        # Provoke CRC over length
        # There exists no CRC of such a length so we simply bullshit our way out
        crc32 = crcmod.predefined.Crc('crc-32')
        crc32.digest_size = 16
        with self.assertRaises(ValueError):
            rero.Packer(1, crc=crc32)

        # Provoke too many overhead + CRC bytes
        crc64 = crcmod.predefined.Crc('crc-64')
        with self.assertRaises(ValueError):
            rero.Packer(1, enableFrameLenCheck=True, addFrameFieldLen=1, addrFieldLen=4, crc=crc64)
        with self.assertRaises(ValueError):
            rero.Packer(1, enableFrameLenCheck=False, addFrameFieldLen=2, addrFieldLen=4, crc=crc64)

        # Provoke no source address but address field
        with self.assertRaises(ValueError):
            rero.Packer(addrFieldLen=1)

        # Provoke source address but no address field
        with self.assertRaises(ValueError):
            rero.Packer(1, addrFieldLen=0)

        # Add a log handler
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # add formatter to ch
        ch.setFormatter(formatter)

        # Setup rero
        reroCoder = rero.Packer(1, 10)
        reroCoder.add_logger_handler(ch)
        reroCoder.remove_logger_handler(ch)

        # Different CRC: CRC-8
        reRoCoder = rero.Packer(1, 10, crc=crcmod.predefined.Crc('crc-8'))
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        self.assertEqual(msgToSend, b"$\x05\x01\x05\x01\x02\x03\x04'\x00")

    def test_unpack(self):
        # Normal usage
        reRoCoder = rero.Packer(1, 10)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Other recipient - no exception raised
        reRoCoder = rero.Packer(1, 10)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 2)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payloadRcd, b'')

        # Other recipient - exception raised
        reRoCoder = rero.Packer(1, 10, yieldOtherRecipientNotification=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 2)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.OtherRecipientReroNotification))

        # CRC Error - no exception raised
        reRoCoder = rero.Packer(1, 10)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        tmp = bytearray(msgToSend)
        tmp[-2] = 11
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payloadRcd, b'')

        # CRC Error - exception raised
        reRoCoder = rero.Packer(1, 10, yieldCRCError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        tmp = bytearray(msgToSend)
        tmp[-2] = 11
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.CRCReroError))

        # Out of sync error
        reRoCoder = rero.Packer(1, 10, yieldOutOfSyncError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload)
        tmp = bytearray(msgToSend)
        tmp[-1] = 11
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.OutOfSyncReroError))

        # Different CRC: CRC-16
        reRoCoder = rero.Packer(1, 10, crc=crcmod.predefined.Crc('crc-16'))
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payloadRcd, payload)

        # CRC Error - exception raised for CRC-16
        reRoCoder = rero.Packer(1, 10, crc=crcmod.predefined.Crc('crc-16'), yieldCRCError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        tmp = bytearray(msgToSend)
        tmp[-2] = 11
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.CRCReroError))

        # Different CRC: CRC-8
        reRoCoder = rero.Packer(1, 10, crc=crcmod.predefined.Crc('crc-8'), yieldCRCError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Different CRC: CRC-64
        reRoCoder = rero.Packer(1, 10, crc=crcmod.predefined.Crc('crc-64'), yieldCRCError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Test no address field
        reRoCoder = rero.Packer(addrFieldLen=0)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Test no CRC
        reRoCoder = rero.Packer(1, crc=None)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Test absolute minimum: no CRC, no address field, no extra frame length field
        reRoCoder = rero.Packer(crc=None, addrFieldLen=0, addFrameFieldLen=0)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Test two messages in a row
        reRoCoder = rero.Packer(1, 10)
        payload = bytes([1, 2, 3, 4])
        payload2 = bytes([2, 2, 3, 4])
        payload3 = bytes([3, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1) + reRoCoder.pack(payload2, 1) + reRoCoder.pack(payload3, 1)

        for cnt, payloadRcd in enumerate(reRoCoder.unpack(msgToSend)):
            if cnt == 0:
                self.assertEqual(payload, payloadRcd)

            if cnt == 1:
                self.assertEqual(payload2, payloadRcd)

            if cnt == 2:
                self.assertEqual(payload3, payloadRcd)

        # Test resynchronization
        reRoCoder = rero.Packer(1, 10, yieldOutOfSyncError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        tmp = bytearray(msgToSend)
        tmp[-1] = 11
        tmp.append(20)
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.OutOfSyncReroError))
        msgToSend2 = reRoCoder.pack(payload, 1)
        # Add intermediate EOF delimiter
        msgToSend2 = bytes([9, 9, 9, 0, 0]) + msgToSend2
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend2):
            pass
        self.assertEqual(payload, payloadRcd)

        # Test length field checksum
        reRoCoder = rero.Packer(1, 10, enableFrameLenCheck=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertEqual(payload, payloadRcd)

        # Provoke synchronization error - set byte in header to zero
        reRoCoder = rero.Packer(1, 10, yieldOutOfSyncError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        tmp = bytearray(msgToSend)
        tmp[1] = 0
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.OutOfSyncReroError))

        # Provoke length field error
        reRoCoder = rero.Packer(1, 10, enableFrameLenCheck=True, yieldOutOfSyncError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        tmp = bytearray(msgToSend)
        tmp[2] = 11
        msgToSend = bytes(tmp)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.OutOfSyncReroError))

        # Provoke timeout error
        reRoCoder = rero.Packer(1, 10, timeoutPeriod=0.5, yieldTimeoutError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        msgToSend1 = msgToSend[:-2]                         # clip last two bytes
        msgToSend2 = msgToSend[-2:]                         # remaining bytes
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend1):     # put partly received frame into unpack()
            pass
        self.assertEqual(b'', payloadRcd)                   # assume nothing comes back
        sleep(0.6)                                          # sleep too long
        payloadRcd = b''                                    # check
        for payloadRcd in reRoCoder.unpack(msgToSend2):
            pass
        self.assertTrue(isinstance(payloadRcd, rero.TimeoutReroError))

        # Send normal message, provoke timeout error and send normal message again
        reRoCoder = rero.Packer(1, 10, timeoutPeriod=0.5, yieldTimeoutError=True)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        msgToSend1 = msgToSend[:-2]                         # clip last two bytes
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):      # unpack normal message
            pass
        self.assertEqual(payload, payloadRcd)
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend1):     # put partly received frame into unpack()
            pass
        self.assertEqual(b'', payloadRcd)                   # assume nothing comes back
        sleep(0.6)                                          # sleep too long
        payloadRcd = b''                                    # check
        for payloadRcd in reRoCoder.unpack(b''):            # for the second part we do not put anything new into
            # unpack() to flush its receive buffer - otherwise part one will be flushed by the timeout but part 2
            # will remain and make the following new frame invalid
            pass
        self.assertTrue(isinstance(payloadRcd, rero.TimeoutReroError))
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend):      # unpack normal message
            pass
        self.assertEqual(payload, payloadRcd)

        # Send message, where the first part contains only part of the header
        reRoCoder = rero.Packer(1, 10)
        payload = bytes([1, 2, 3, 4])
        msgToSend = reRoCoder.pack(payload, 1)
        msgToSend1 = msgToSend[:2]  # clip to fist two bytes
        msgToSend2 = msgToSend[2:]  # remaining bytes
        payloadRcd = b''
        for payloadRcd in reRoCoder.unpack(msgToSend1):  # put partly received frame into unpack()
            pass
        self.assertEqual(b'', payloadRcd)  # assume nothing comes back
        for payloadRcd in reRoCoder.unpack(msgToSend2):
            pass
        self.assertEqual(payload, payloadRcd)
